# OpenHarmony Release Notes

-   [OpenHarmony 1.1.0 LTS \(2021-04-27\)](openharmony-1-1-0-lts.md)
-   [OpenHarmony 1.0 \(2020-09-10\)](openharmony-1-0.md)

